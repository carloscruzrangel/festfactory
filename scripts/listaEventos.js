const eventsDiv = document.getElementById('ListaEvento');
const sitesDiv = document.getElementById('ListaSitio');

/** Pide la lista de eventos y su tabla */
function generateEventsList() {
  let xhr = new XMLHttpRequest();
  xhr.open('GET', url + '/events', true);
  xhr.setRequestHeader('Authorization', localStorage.getItem('TOKEN'));
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        generateEventsTable(JSON.parse(xhr.responseText));
      } else {
        //alert(':(');
      }
    }
  };
  xhr.send();
}

/** Pide la lista de sitios y su tabla */
function generateSitesList() {
  let xhr = new XMLHttpRequest();
  xhr.open('GET', url + '/sites', true);
  xhr.setRequestHeader('Authorization', localStorage.getItem('TOKEN'));
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        generateSitesTable(JSON.parse(xhr.responseText));
      } else {
        //alert(':(');
      }
    }
  };
  xhr.send();
}

function deleteEvent(eventId) {
  let xhr = new XMLHttpRequest();
  xhr.open('DELETE', url + '/events/' + eventId, true);
  xhr.setRequestHeader(
    'Authorization',
    localStorage.getItem('TOKEN')
  );
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        window.location.reload();
      } else {
        // Manejar el caso de error de la solicitud
      }
    }
  };
  xhr.send();
}

function viewCronograma(eventId) {
  var url = 'cronograma.html?id=' + encodeURIComponent(eventId);
    window.location.href = url;
}

function editEvent(eventId) {
  var url = 'editarEvento.html?id=' + encodeURIComponent(eventId);
  window.location.href = url;
}

function deleteSite(siteId) {
  let xhr = new XMLHttpRequest();
  xhr.open('DELETE', url + '/sites/' + siteId, true);
  xhr.setRequestHeader(
    'Authorization',
    localStorage.getItem('TOKEN')
  );
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        window.location.reload();
      } else {
        // Manejar el caso de error de la solicitud
      }
    }
  };
  xhr.send();
}

function generateEventsTable(jsonData) {
  let table = document.createElement('table');
  let thead = document.createElement('thead');
  let tbody = document.createElement('tbody');

  // Crear encabezado de la tabla
  let headerRow = document.createElement('tr');
  let headers = ['ID', 'Nombre', 'Fecha', 'Hora de inicio', 'Hora de fin', '', '', ''];
  headers.forEach(function (headerText) {
    let header = document.createElement('th');
    header.textContent = headerText;
    headerRow.appendChild(header);
  });
  thead.appendChild(headerRow);
  table.appendChild(thead);

  // Crear filas de empleados
  jsonData.forEach(function (event) {
    let row = document.createElement('tr');
    let idCell = document.createElement('td');
    idCell.textContent = event.id;
    let nameCell = document.createElement('td');
    nameCell.textContent = event.name;
    let dateCell = document.createElement('td');
    dateCell.textContent = event.date;
    let startCell = document.createElement('td');
    startCell.textContent = event.start_time;
    let endCell = document.createElement('td');
    endCell.textContent = event.end_time;
    let editarCell = document.createElement('td');
    let cronogramaCell = document.createElement('td');
    let deleteCell = document.createElement('td');

    let editarButton = document.createElement('button');
    editarButton.textContent = 'Editar'
    editarButton.classList.add('botonEditar')

    let cronogramaButton = document.createElement('button');
    cronogramaButton.textContent = 'Cronograma'
    cronogramaButton.classList.add('botonCronograma')

    let deleteButton = document.createElement('button');
    deleteButton.textContent = 'Eliminar';
    deleteButton.classList.add('botonEliminar')

    cronogramaButton.addEventListener('click', function() {
      viewCronograma(event.id)
    });

    editarButton.addEventListener('click', function() {
      editEvent(event.id)
    });

    deleteButton.addEventListener('click', function () {
      deleteEvent(event.id);
    });

    editarCell.appendChild(editarButton)
    cronogramaCell.appendChild(cronogramaButton)
    deleteCell.appendChild(deleteButton)

    row.appendChild(idCell);
    row.appendChild(nameCell);
    row.appendChild(dateCell);
    row.appendChild(startCell);
    row.appendChild(endCell);
    row.appendChild(cronogramaCell);
    row.appendChild(editarCell);
    row.appendChild(deleteCell);
    tbody.appendChild(row);
  });

  table.appendChild(tbody);
  eventsDiv.appendChild(table);
}

function generateSitesTable(jsonData) {
  let table = document.createElement('table');
  let thead = document.createElement('thead');
  let tbody = document.createElement('tbody');

  // Crear encabezado de la tabla
  let headerRow = document.createElement('tr');
  let headers = ['Nombre', 'Lat', 'Lon', 'Areas', ''];
  headers.forEach(function (headerText) {
    let header = document.createElement('th');
    header.textContent = headerText;
    headerRow.appendChild(header);
  });
  thead.appendChild(headerRow);
  table.appendChild(thead);

  // Crear filas de empleados
  jsonData.forEach(function (site) {
    let row = document.createElement('tr');
    /*var idCell = document.createElement('td');
    idCell.textContent = event.id;*/
    let nameCell = document.createElement('td');
    nameCell.textContent = site.name;
    let latCell = document.createElement('td');
    latCell.textContent = site.location.lat;
    let lonCell = document.createElement('td');
    lonCell.textContent = site.location.lon;
    let areasCell = document.createElement('td');
    let areas = ''
    site.areas.forEach(function (area) {
      areas += area.name + '<br>'
    });
    areasCell.innerHTML = areas;
    let deleteCell = document.createElement('td');

    /*var editarButton = document.createElement('button');
    editarButton.textContent = 'Editar'
    editarButton.classList.add('botonEditar')

    editarButton.addEventListener('click', function() {
      editSite(site.id)
    });*/

    let deleteButton = document.createElement('button');
    deleteButton.textContent = 'Eliminar';
    deleteButton.classList.add('botonEliminar')

    deleteButton.addEventListener('click', function () {
      deleteSite(site.id);
    });

    //editarCell.appendChild(editarButton)
    //cronogramaCell.appendChild(cronogramaButton)
    deleteCell.appendChild(deleteButton)

    //row.appendChild(idCell);
    row.appendChild(nameCell);
    row.appendChild(latCell);
    row.appendChild(lonCell);
    row.appendChild(areasCell);
    //row.appendChild(editarCell);
    row.appendChild(deleteCell);
    tbody.appendChild(row);
  });

  table.appendChild(tbody);
  sitesDiv.appendChild(table);
}

window.onload = function() {
  generateEventsList();
  generateSitesList();
}

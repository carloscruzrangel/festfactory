  const xhr = new XMLHttpRequest();
  xhr.open('Tipo_de_pedición', url+'', true);
  
  //Si necesitas autenticar
  xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('TOKEN'));
  
  //Si necesitas body
  var jsonData = JSON.stringify({Un_JSON});
  xhr.setRequestHeader('Content-type', 'application/json');
  
  xhr.onreadystatechange = function () {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        if (xhr.status === El_estado_de_éxito) {
          console.log(JSON.parse(xhr.responseText));
        } else {
          //NO hay éxito
        }
      }
    };
  
  //sin body
  xhr.send();
  //con body
  xhr.send();

  // Crea el mapa en el contenedor "mapContainer" con una ubicación y nivel de zoom iniciales
  var map = L.map('mapContainer').setView([35.695547, 139.7009607], 13);

  // Agrega un mapa base utilizando OpenStreetMap
  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
    maxZoom: 18,
  }).addTo(map);

  // Crea una capa de calor utilizando los datos de latitud y longitud de los sitios
  var heatLayer = L.heatLayer([
    [35.695547, 139.7009607], // Ejemplo de coordenadas de sitio
    // Agrega más coordenadas de sitios aquí...
  ]).addTo(map);





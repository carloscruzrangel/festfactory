const xhr = new XMLHttpRequest();
const buttonSite = document.getElementById('accept-site');
const addSiteForm = document.getElementById('form-content');
const buttonDiv = document.getElementById('area-buttons');
let areas = [];
let areaNumber = 0;

// Agregar sitio de escucha al formulario con el id 'agregar-sitio'
addSiteForm.addEventListener('submit', function (event) {
  event.preventDefault(); // Evita el envío del formulario
  buttonSite.disabled = true;

  let jsonData = JSON.stringify({
    name: document.getElementById('name_site').value,
    location: {
      lat: document.getElementById('lat_site').value,
      lon: document.getElementById('lon_site').value
    }
  });

  createSite(jsonData)
});

function createSite(jsonData) {
  xhr.open('POST', url + '/sites', true);
  xhr.setRequestHeader('Content-type', 'application/json');
  xhr.setRequestHeader('Authorization', localStorage.getItem('TOKEN'));

  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 201) {
        if (areas.length > 0) {
          let jsonAreas = []
          for(let i = 0; i < areas.length; i++) {
            let jsonArea = {
              name: document.getElementById('n_area_' + String(i)).value,
              size: {
                width: document.getElementById('w_area_' + String(i)).value,
                depth: document.getElementById('d_area_' + String(i)).value,
                height: document.getElementById('h_area_' + String(i)).value
              }
            };
            jsonAreas.push(jsonArea);
          }
          console.log(jsonAreas);
          console.log(JSON.stringify({areas: jsonAreas}));
          createAreas(
            JSON.parse(xhr.responseText).id,
            JSON.stringify({areas: jsonAreas})
          );
        }
        addSiteForm.reset();
      } else {
        alert('fail');
      }
      buttonSite.disabled = false;
    }
  };
  xhr.send(jsonData);
}

function createAreas(site_id, jsonData) {
  xhr.open('POST', url +'/sites/'+ site_id +'/areas', true);
  xhr.setRequestHeader('Content-type', 'application/json');
  xhr.setRequestHeader('Authorization', localStorage.getItem('TOKEN'));

  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 201) {
        while(areas.length != 0) {
          addSiteForm.removeChild(areas.pop());
        }
        areaNumber = 0;
      } else {}
    }
  };
  xhr.send(jsonData);
}

document.getElementById('newArea').addEventListener('click', function () {
  var area = document.createElement('div')
  area.classList.add('form-box');
  area.innerHTML = '<label class="IDlabel label"> Area ' + String(areaNumber + 1) + '</label>';
  let areaN = 'n_area_' + String(areaNumber);
  let areaW = 'w_area_' + String(areaNumber);
  let areaD = 'd_area_' + String(areaNumber);
  let areaH = 'h_area_' + String(areaNumber);
  area.innerHTML += '<input class="controls" type="text" name="'+ areaN +'" id="'+ areaN +'" placeholder="Nombre" required />'

  var dim = document.createElement('div');
  dim.classList.add('dimensions');
  dim.innerHTML = '<input class="controls" type="number" name="'+ areaW +'" id="'+ areaW +'" placeholder="Ancho" required />'
  dim.innerHTML += '<input class="controls" type="number" name="'+ areaD +'" id="'+ areaD +'" placeholder="Largo" required />'
  dim.innerHTML += '<input class="controls" type="number" name="'+ areaH +'" id="'+ areaH +'" placeholder="Alto" required />'

  area.appendChild(dim);
  addSiteForm.insertBefore(area, buttonDiv);
  areas.push(area);
  areaNumber++;
});

document.getElementById('rmArea').addEventListener('click', function () {
  if(areaNumber == 0) return;
  addSiteForm.removeChild(areas.pop());
  areaNumber--;
});

document.getElementById('volverBtn').addEventListener('click', function () {
  window.location.href = './listaEventos.html';
});

document.getElementById('irBtn').addEventListener('click', function () {
  window.location.href = './agregarEvento.html';
});

/* AGREGA EMPLEADO
 * Este código realiza una solicitud HTTP POST a un punto final de la API
 * para agregar un nuevo empleado a una base de datos. También evita el
 * comportamiento de envío predeterminado del formulario, obtiene los valores
 * de los campos de entrada, crea un objeto JSON con los datos, establece
 * las cabeceras adecuadas para la solicitud y maneja la respuesta. Además,
 * agrega un escuchador de eventos a un botón para redirigir al usuario a
 * una página diferente cuando se hace clic.
 */

const xhr = new XMLHttpRequest();
const title = document.getElementById('title');
var key = 'D0rurar1_Mus5u_Na4n4';
let id = decodeURIComponent(new URLSearchParams(window.location.search).get('id'));
let plaintext = CryptoJS.AES.decrypt(id, key).toString(CryptoJS.enc.Utf8);

crudEmployeeForm = document.getElementById('crud-employee');

crudEmployeeForm.addEventListener('submit', function (event) {
  event.preventDefault(); // Evita el envío del formularios

  // Creación del objeto de datos a enviar en la solicitud
  let jsonData = JSON.stringify({
    name: document.getElementById('nombre').value,
    first_surname: document.getElementById('apellido_paterno').value,
    second_surname: document.getElementById('apellido_materno').value,
  });

  if(plaintext.length < 1) {
    addEmployee(jsonData);
  } else {
    editEmployee(jsonData);
  }
});

function addEmployee(jsonData) {
  // Configuración y envío de la solicitud POST
  xhr.open('POST', url + '/employees', true);
  xhr.setRequestHeader('Content-type', 'application/json');
  xhr.setRequestHeader(
    'Authorization',
    localStorage.getItem('TOKEN')
  );

  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 201) {
        alert('exito');
      } else {
        alert('fail');
        // No login. Hacer algo para que el usuario se dé cu
      }
    }
  };
  xhr.send(jsonData);
}

function editEmployee(jsonData) {
/* These lines of code are setting up and sending an HTTP PUT request to a server. */
  xhr.open('PUT', url + '/employees/' + plaintext, true);
  xhr.setRequestHeader('Content-type', 'application/json');
  xhr.setRequestHeader(
    'Authorization',
    localStorage.getItem('TOKEN')
  );

  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        alert('exito');
      } else {
        alert('fail');
      }
    }
  };
  // Enviar la solicitud PUT con los datos en formato JSON
  xhr.send(jsonData);
}

const volverBtn = document.getElementById('volverBtn');
volverBtn.addEventListener('click', function () {
  window.location.href = './listaEmpleados.html';
});

function getItemProperties() {
  xhr.open('GET', url + '/employees/' + plaintext, false);
  xhr.setRequestHeader(
    'Authorization',
    localStorage.getItem('TOKEN')
  );
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        let json = JSON.parse(xhr.responseText);
        document.getElementById('nombre').value = json.name;
        document.getElementById('apellido_paterno').value = json.first_surname;
        document.getElementById('apellido_materno').value = json.first_surname;
      } else {
        window.location.href = './login.html';
      }
    }
  };
  xhr.send();
}

window.onload = function() {
  if(plaintext.length > 1) {
    title.innerText = 'Editar Empleado'
    getItemProperties()
  } else {
    title.innerText = 'Agregar Empleado'
  }
};

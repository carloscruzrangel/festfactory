/**
 * Varias funciones de verificacion micelaneas
 */

function validTicket(text) {
  var patron = /^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/;
  return patron.test(text);
}

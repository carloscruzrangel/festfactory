/* BUSCAR EVENTOS
 * Este código realiza una solicitud GET a una API para obtener eventos y generar una lista de eventos en el DOM.
 * Utiliza el objeto XMLHttpRequest para realizar la solicitud y obtener la respuesta de la API.
 * Al cargar la página, se llama a la función getEvents para iniciar la obtención de eventos.
 * Los eventos obtenidos se procesan en la función generateList, que crea elementos HTML y los agrega al DOM.
 */

// Declaración de variables
const xhr = new XMLHttpRequest();
const boxes = document.getElementById('boxes');
var key = 'pep3Peca5_PicapaP4s';

// Función para obtener eventos
function getEvents() {
  xhr.open('GET', url + '/events', true);
  xhr.setRequestHeader(
    'Authorization',
    localStorage.getItem('TOKEN')
  );
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        generateList(JSON.parse(xhr.responseText));
      } else {} // Manejar el caso de error de la solicitud
    }
  };
  xhr.send();
}

// Función para generar la lista de eventos
function generateList(events) {
  events.forEach((event, index) => {
    // Crear elementos HTML para cada evento
    let divElement = document.createElement('div');
    divElement.classList.add('image-zoom', 'box');
    let inputElement = document.createElement('input');
    inputElement.type = 'image';
    inputElement.src = '../img/lorem-pixel.png';
    inputElement.onclick = function () {
      var encrypted = CryptoJS.AES.encrypt(String(event.id), key);
      window.location.href = './evento.html?id=' + encodeURIComponent(encrypted).replace(/%20/g, '+');
    };
    inputElement.alt = 'img-1';

    let captionElement = document.createElement('div');
    captionElement.classList.add('image-caption');
    captionElement.textContent = event.name;

    // Agregar los elementos al DOM
    divElement.appendChild(inputElement);
    divElement.appendChild(captionElement);

    boxes.appendChild(divElement);
  });
}

// Llamar a la función getEvents al cargar la página
window.onload = getEvents;
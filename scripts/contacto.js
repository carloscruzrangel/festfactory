/* The above code is a JavaScript file that is used to create a web  *
 * page that generate a faq                                          */

// Section data
const contacts = [
  { name: 'facebook', src: '../img/facebook.png', info: 'https://facebook/empresita' },
  { name: 'gmail', src: '../img/gmail.png', info: 'correo-1@empresa.com<br>correo-2@empresa.com' },
  { name: 'twitter', src: '../img/twitter.png', info: '@empresita' },
  { name: 'whatsapp', src: '../img/whatsapp.png', info: '5555555555<br>8888888888' },
  // Add more
];

//Fuction to generate contacts
function generateContacts() {
  var main = document.getElementById('content');

  contacts.forEach((source, index) => {
    var contact = document.createElement('div');
    contact.classList.add('contact');

    var img = document.createElement('img');
    img.src = source.src;
    img.alt = source.name;
    contact.append(img);

    var info = document.createElement('div');
    info.classList.add('info');
    var h1 = document.createElement('h1');
    h1.textContent = source.name;
    info.append(h1);
    var p = document.createElement('p');
    p.innerHTML = source.info;
    info.append(p);
    contact.append(info);

    main.append(contact);
  });
}

// Generate contacts on load
window.onload = generateContacts;

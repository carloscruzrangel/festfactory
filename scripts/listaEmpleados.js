const xhr = new XMLHttpRequest();
var key = 'D0rurar1_Mus5u_Na4n4';

function generateList() {
  xhr.open('GET', url + '/employees', true);
  xhr.setRequestHeader(
    'Authorization',
    localStorage.getItem('TOKEN')
  );
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        generateTable(JSON.parse(xhr.responseText));
      } else {
        //alert(':(');
      }
    }
  };
  xhr.send();
}

function deleteEmployee(employeeId) {
  xhr.open('DELETE', url + '/employees/' + employeeId, true);
  xhr.setRequestHeader(
    'Authorization',
    localStorage.getItem('TOKEN')
  );
  xhr.onreadystatechange = function () {
  if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        window.location.reload();
      } else {
        // Manejar el caso de error de la solicitud
      }
    }
  };
  xhr.send();
}

function generateTable(json) {
  var table = document.createElement('table');
  var thead = document.createElement('thead');
  var tbody = document.createElement('tbody');

  // Crear encabezado de la tabla
  var headerRow = document.createElement('tr');
  var headers = [/*'ID', */'Nombre', 'Apellido Paterno', 'Apellido Materno', '', ''];
  headers.forEach(function (headerText) {
    var header = document.createElement('th');
    header.textContent = headerText;
    headerRow.appendChild(header);
  });
  thead.appendChild(headerRow);
  table.appendChild(thead);

  // Crear filas de empleados
  json.forEach(function (employee) {
    var row = document.createElement('tr');
    /*var idCell = document.createElement('td');
    idCell.textContent = employee.id;*/
    var nameCell = document.createElement('td');
    nameCell.textContent = employee.name;
    var lastNameCell = document.createElement('td');
    lastNameCell.textContent = employee.first_surname;
    var middleNameCell = document.createElement('td');
    middleNameCell.textContent = employee.second_surname;
    var updateCell = document.createElement('td');
    var deleteCell = document.createElement('td');

    var updateButton = document.createElement('button');
    updateButton.textContent = 'Editar';
    updateButton.classList.add('botonEditar')

    var deleteButton = document.createElement('button');
    deleteButton.textContent = 'Eliminar';
    deleteButton.classList.add('botonEliminar')

    updateButton.addEventListener('click', function() {
      var encrypted = CryptoJS.AES.encrypt(String(employee.id), key);
      window.location.href = './crudEmpleado.html?id=' + encodeURIComponent(encrypted).replace(/%20/g, '+');
    });

    deleteButton.addEventListener('click', function () {
      deleteEmployee(employee.id);
    });

    updateCell.appendChild(updateButton);
    deleteCell.appendChild(deleteButton);

    //row.appendChild(idCell);
    row.appendChild(nameCell);
    row.appendChild(lastNameCell);
    row.appendChild(middleNameCell);
    row.appendChild(updateCell);
    row.appendChild(deleteCell);
    tbody.appendChild(row);
  });

  table.appendChild(tbody);
  document.getElementById('ListaEmpleados').appendChild(table);
}

// Generate contacts on load
window.onload = generateList;

const xhr = new XMLHttpRequest();

/*const sites = document.getElementById('sitio');

function loadSites() {
  xhr.open('GET', url + '/sites', true);
  xhr.setRequestHeader('Authorization', localStorage.getItem('TOKEN'));
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        let siteList = JSON.parse(xhr.responseText);
        if(siteList.length == 0) return;
        sites.innerHTML = '';
        let opt = document.createElement('option');
        opt.value = -1;
        opt.textContent = 'Seleccione un sitio'
        sites.appendChild(opt)
        siteList.forEach(function (site) {
          let option = document.createElement('option');
          option.value = site.id;
          option.textContent = site.name;
          sites.appendChild(option);
        });
      }
    }
  };
  xhr.send();
}*/
// Agregar evento de escucha al formulario con el id 'agregar-evento'
document.getElementById('editar-evento').addEventListener('submit', function (event) {
  event.preventDefault(); // Evita el envío del formulario

  var url_page = new URL(window.location.href);
  var id = url_page.searchParams.get('id');

  // Obtener los valores de los campos del formulario
  var nombre = document.getElementById('nombre').value;
  var fecha = document.getElementById('fecha').value;
  var inicio = document.getElementById('hora_inicio').value;
  var fin = document.getElementById('hora_fin').value;
  var descripcion = document.getElementById('descripcion').value;

  // Crear un objeto de datos con los valores obtenidos
  var data = {
    date: fecha,
    start_time: inicio,
    end_time: fin,
    name: nombre,
    description: descripcion
  };

  var jsonData = JSON.stringify(data);

  // Configurar la solicitud PUT
  xhr.open('PUT', url + '/events/' + id, true);
  xhr.setRequestHeader('Content-type', 'application/json');
  xhr.setRequestHeader('Authorization', localStorage.getItem('TOKEN'));

  // Manejar el estado de la solicitud
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        alert('exito');
      } else {
        alert('fail');
        // No login. Hacer algo para que el usuario se dé cuenta
      }
    }
  };

  // Enviar la solicitud PUT con los datos en formato JSON
  xhr.send(jsonData);
});

const volverBtn = document.getElementById('volverBtn');
volverBtn.addEventListener('click', function () {
  window.location.href = './listaEventos.html';
});

//window.onload = loadSites

const xhr = new XMLHttpRequest();
const events = document.getElementById('evento');
const cerrar_sesion = document.getElementById('cerrar-sesion');
const checkin = document.getElementById('checkin');
const IDField = document.getElementById('IDField')
checkin.disabled = true;

function requestUser() {
  xhr.open('GET', url + '/users/1', true);
  xhr.setRequestHeader('Authorization', localStorage.getItem('TOKEN'));
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        var response = JSON.parse(xhr.responseText);
        console.log(response);
      } else {
        window.location.href = './login.html';
      }
    }
  };
  xhr.send();
}

function loadEvents() {
  xhr.open('GET', url + '/events', true);
  xhr.setRequestHeader('Authorization', localStorage.getItem('TOKEN'));
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        let opt = document.createElement('option');
        opt.value = -1;
        opt.textContent = 'Seleccione un evento'
        events.appendChild(opt)
        let eventList = JSON.parse(xhr.responseText);
        eventList.forEach(function (event) {
          let option = document.createElement('option');
          option.value = event.id;
          option.textContent = event.name;
          events.appendChild(option);
        });
      }
    }
  };
  xhr.send();
}

cerrar_sesion.addEventListener('click', function(event) {
  event.preventDefault();
  sessionStorage.removeItem('TOKEN');
  localStorage.removeItem('TOKEN');
  window.location.href = './login.html'
})


// Prevenir de ticket mal formado
IDField.addEventListener('input', function() {
  checkin.disabled = !validTicket(IDField.value.trim());
});

// Deshabilitar el botón si no se ha seleccionado ninguna opción en el campo de selección
events.addEventListener('change', function() {
  checkin.disabled = (events.value == -1);
});

document.addEventListener('DOMContentLoaded', function() {
  var openModalBtn = document.getElementById('openModalBtn');
  var closeModalBtn = document.getElementById('closeModalBtn');
  var modal = document.getElementById('modal');
  let modalResponse = document.getElementById('modalResponse');
  let closeModalResponseBtn = document.getElementById('closeModalResponseBtn');
  let response = document.getElementById('response');
  let pResponse = document.getElementById('pResponse');
  let verificationForm = document.getElementById('verificationForm');

  openModalBtn.addEventListener("click", function() {
    modal.style.display = "block";
  });

  closeModalBtn.addEventListener("click", function() {
    modal.style.display = "none";
  });

  closeModalResponseBtn.addEventListener("click", function() {
    modalResponse.style.display = 'none';
  });

  window.addEventListener("click", function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
    if (event.target == modalResponse) {
      modalResponse.style.display = 'none';
    }
  });

  verificationForm.addEventListener("submit", function(event) {
    event.preventDefault();
    modalResponse.style.display = 'block';
    xhr.open('GET', url + '/events/' + events.value +'/tickets', true);
    xhr.setRequestHeader('Authorization', localStorage.getItem('TOKEN'));
    xhr.onreadystatechange = function () {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        modal.style.display = 'none';
        modalResponse.style.display = 'block';
        if (xhr.status === 200) {
          let tickets = JSON.parse(xhr.responseText);
          let ticket = tickets.find((item) => item.uuid == IDField.value.trim());
          if (ticket != null) {
            response.innerText = 'Boleto Encontrado';
            pResponse.innerText = 'Disfrute la fiesta wuuuuuuuu';
          } else {
            response.innerText = 'Boleto NO Encontrado';
            pResponse.innerText = 'A pelar papas';
          }
        } else {
          response.innerText = 'No se pudo obtener la lista de boletos';
        }
      }
    };
    xhr.send();
  });
});

window.onload = function() {
  requestUser();
  loadEvents();
};

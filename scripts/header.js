/* *******************************************************************************
 * The code is creating and inserting a header and footer into an HTML document.
 * It also includes eventlisteners for a profile button and window closing, as well
 * as setting and updating a window count in local storage.
 ******************************************************************************** */

/* Variable global */
const url = 'https://api.diu-server.net/v1'

/* Elementos del Header y Footer */
const mainElement = document.querySelector("main");
const parentElement = mainElement.parentNode;

/* ----------------------------- */
/*            HEADER             */
/* ----------------------------- */

// Crea el elemento de imagen del logo
const logoImg = document.createElement('img');
logoImg.className = 'logo';
logoImg.src = '../img/FF-Logo-Transparent.png';
logoImg.alt = 'logo';

// Crea el elemento del encabezado
const header = document.createElement('header');

header.innerHTML = `
<div id="bar">
  <div></div>
  <a class="left" href="./inicio.html">Inicio</a>
  <a class="right" href="./buscarEventos.html">Eventos</a>
  <a class="right" href="./ayuda.html">Ayuda</a>
  <div></div>
  <input type="image" src="../img/perfil.png" id="perfilButton" alt="perfil">
</div>
`;

// Inserta el encabezado en el documento
parentElement.insertBefore(logoImg, mainElement);
parentElement.insertBefore(header, mainElement);

// Funcion del boton de Usuario
const perfilButton = document.getElementById("perfilButton");
perfilButton.addEventListener("click", function () {
  if (localStorage.getItem("TOKEN") == null) {
    window.location.href = "./login.html";
  } else {
    let vrif = new XMLHttpRequest();
    vrif.open('GET', url + '/sites', true);
    vrif.setRequestHeader('Authorization', localStorage.getItem('TOKEN'));
    vrif.onreadystatechange = function () {
      if (vrif.readyState === XMLHttpRequest.DONE) {
        if (vrif.status === 200) {
          window.location.href = './perfil.html';
        } else {
          sessionStorage.removeItem('TOKEN');
          localStorage.removeItem('TOKEN');
          window.location.href = './login.html';
        }
      }
    };
    vrif.send();
  }
});

/* ----------------------------- */
/*            FOOTER             */
/* ----------------------------- */

// Crear el footer
const footer = document.createElement('footer');

// Agregar contenido al footer
footer.innerHTML = `
  <div class="page-footer-grid">
    <div class="page-footer-nav-col-1">
      <a class="footer" href="./buscarEventos.html">Buscar eventos</a>
    </div>
    <div class="page-footer-nav-col-2">
      <a class="footer" href="./ayuda.html">Ayuda</a>
    </div>
    <div class="page-footer-nav-col-3">
      <a class="footer" href="./contacto.html">Contacto</a>
    </div>
  </div>
  <div class="company-footer">
    <img class="company" src="../img/FF-Logo.png" alt="company">
    <hr id="vertical-line">
    <span style="color: white; font-size: var(--tiny-font);">FestFactory</span>
  </div>
`;

// Insertar el footer antes del elemento <main>
parentElement.insertBefore(footer, mainElement.nextSibling);

/* ----------------------------------- */
/* CONTEO DE VENTANAS EN LOCAL STORAGE */
/* ----------------------------------- */

// Variable de conteo en el Local Storage
let windowCount = localStorage.getItem('windowCount');

if(sessionStorage.getItem('TOKEN') != null) {
  localStorage.setItem('TOKEN',sessionStorage.getItem('TOKEN'));
}
if (windowCount == null || windowCount < 0) {
  localStorage.setItem("windowCount", 0);
}
windowCount = localStorage.getItem("windowCount");
windowCount++;
localStorage.setItem("windowCount", windowCount);

// Agregar event listener para capturar el cierre de la ventana
window.addEventListener('beforeunload', function (event) {
  let newWindowCount = localStorage.getItem("windowCount") - 1;
  if (newWindowCount === 0) {
    if(localStorage.getItem('TOKEN') != null) {
      sessionStorage.setItem('TOKEN',localStorage.getItem('TOKEN'));
    }
    localStorage.removeItem("TOKEN");
    localStorage.removeItem("windowCount");
  } else {
    localStorage.setItem("windowCount", newWindowCount);
  }
});

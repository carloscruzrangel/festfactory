const xhr = new XMLHttpRequest();
const title = document.getElementById('title');
var key = 'Un_L4mb0rgh11_vE105';
let id = decodeURIComponent(new URLSearchParams(window.location.search).get('id'));
let plaintext = CryptoJS.AES.decrypt(id, key).toString(CryptoJS.enc.Utf8);

crudItemForm = document.getElementById('crud-item')

crudItemForm.addEventListener('submit', function(event) {
  event.preventDefault();

  let jsonData = JSON.stringify({
    name: document.getElementById('name').value,
    description: document.getElementById('description').value,
    size: {
      width: document.getElementById('dimension1').value,
      height: document.getElementById('dimension2').value,
      depth: document.getElementById('dimension3').value
    },
    count: document.getElementById('count').value
  });
  if(plaintext.length < 1) {
    addItem();
  } else {
    editItem();
  }
});

function addItem(jsonData) {
/* These lines of code are setting up and sending an HTTP POST request to a server. */
  xhr.open('POST', url + '/items', true);
  xhr.setRequestHeader('Content-type', 'application/json');
  xhr.setRequestHeader('Authorization', localStorage.getItem('TOKEN'));

  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 201) {
        alert('exito');
        crudItemForm.reset();
      } else {
        alert('fail');
      }
    }
  };
  // Enviar la solicitud POST con los datos en formato JSON
  xhr.send(jsonData);
}


function editItem(jsonData) {
  xhr.open('PUT', url + '/items/' + plaintext, true);
  xhr.setRequestHeader('Content-type', 'application/json');
  xhr.setRequestHeader('Authorization', localStorage.getItem('TOKEN'));

  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        alert('exito');
      } else {
        alert('fail');
      }
    }
  };
  xhr.send(jsonData);
}

const volverBtn = document.getElementById('volverBtn');
volverBtn.addEventListener('click', function () {
  window.location.href = './listaArticulos.html';
});

function getItemProperties() {
  xhr.open('GET', url + '/items/' + plaintext, false);
  xhr.setRequestHeader(
    'Authorization',
    localStorage.getItem('TOKEN')
  );
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        let json = JSON.parse(xhr.responseText);
        document.getElementById('name').value = json.name;
        document.getElementById('description').value = json.description;
        document.getElementById('dimension1').value = json.size.width;
        document.getElementById('dimension2').value = json.size.height;
        document.getElementById('dimension3').value = json.size.depth;
        document.getElementById('count').value = json.count;
      } else {
        window.location.href = './login.html';
      }
    }
  };
  xhr.send();
}

window.onload = function() {
  if(plaintext.length > 1) {
    title.innerText = 'Editar Artículo'
    getItemProperties()
  } else {
    title.innerText = 'Agregar Artículo'
  }
};

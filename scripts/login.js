// Crea una instancia de XMLHttpRequest
const xhr = new XMLHttpRequest();
const usrn = document.getElementById('username-in');
const pswd = document.getElementById('password-in');
const submitButton = document.getElementById('submitButton');

usrn.addEventListener('input', function() {
  submitButton.disabled = usrn.value.trim().length == 0;
});

pswd.addEventListener('input', function() {
  submitButton.disabled = pswd.value.trim().length == 0;
});

// Agrega un evento de escucha al formulario de inicio de sesión cuando se envía
document.getElementById('loginForm').addEventListener('submit', function (event) {
  event.preventDefault(); // Evita el envío del formulario

  // Convierte el objeto de datos a formato JSON
  let jsonData = JSON.stringify({
    username: usrn.value.trim(),
    password: pswd.value.trim(),
    id: 2
  });

  // Abre una solicitud POST hacia la URL de inicio de sesión
  xhr.open('POST', url + '/login', true);

  // Establece el encabezado de la solicitud como 'application/json'
  xhr.setRequestHeader('Content-type', 'application/json');

  // Define una función que se ejecutará cuando cambie el estado de la solicitud
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        // Si la solicitud tiene éxito (estado de respuesta 200)

        // Analiza la respuesta JSON recibida
        var response = JSON.parse(xhr.responseText);


        // Almacena el token en el almacenamiento local del navegador
        let token = 'Bearer ' + response.token;
        localStorage.setItem('TOKEN', token);

        // Redirige al usuario a la página de perfil
        window.location.href = './perfil.html';
      } else {
        console.log(2); // Imprime el valor 2 en la consola (puede ser un código de error personalizado)
      }
    }
  };

  // Envía la solicitud con los datos JSON
  xhr.send(jsonData);
});

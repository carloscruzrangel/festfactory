/* AYUDA
 * Cada sección tiene un título y contenido asociado a la ayuda. Al cargar la página,
 * se llamará a la función generateSections que recorrerá un arreglo de secciones y 
 * creará enlaces en la barra de navegación, así como secciones con sus títulos y 
 * contenido en el cuerpo de la página. 
 */

// Datos de las secciones
const sections = [
  { title: 'Dónde encontrar tus boletos', content: 'En la bandeja de entrada de tu correo electrónico: busca un correo de noreply@order.eventbrite.com en tu bandeja de entrada.<br>En tu cuenta de Eventbrite: cuando te registras en un evento, Eventbrite crea una cuenta asociada a tu dirección de correo electrónico de forma automática. Inicia sesión con la misma dirección de correo y selecciona "Boletos" en el menú desplegable de tu cuenta de Eventbrite.' },
  { title: 'Contáctanos', content: 'Si deseas contactarnos nuestras redes sociales se encuentran en el apartado de contacto, ahí también encontrarás nuestro teléfono de contacto para que te podamos aclarar cualquier duda de algún evento.' },
  { title: 'Sed tortor sem', content: 'Dignissim vel eros nec, lobortis accumsan ante. Aliquam eu ante ligula. Mauris vel urna in magna imperdiet sodales aliquam a metus. Nam sed luctus lacus. Aenean purus mi, lacinia sit amet lacus quis, facilisis sodales sem. Proin sodales luctus nisi quis rutrum. Vivamus augue enim, fermentum non neque a, eleifend accumsan arcu. Suspendisse aliquam neque at enim accumsan commodo. Aliquam ut eros at ex vehicula gravida vitae quis urna. Curabitur dui turpis, lacinia in lorem a, sollicitudin scelerisque augue.' },
  { title: 'Vivamus eget mi vel velit fringilla eleifend nec at enim', content: 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce scelerisque elit tellus, a dictum nisi rhoncus sed. Curabitur sed erat ac arcu facilisis auctor. Praesent fringilla urna eget neque mattis, a porta massa iaculis. Ut molestie dolor id ipsum faucibus vulputate. Etiam fermentum pretium ex, vel aliquet justo. Curabitur ex erat, suscipit ut metus ac, commodo finibus nunc. Nam eleifend dignissim ex, vel vulputate augue. Aliquam ultrices leo eget massa blandit, vel laoreet urna pharetra. Ut velit eros, commodo id aliquet posuere, malesuada ut lacus. Duis convallis faucibus nisi rhoncus aliquam. Ut aliquam lectus sed dapibus fringilla. Pellentesque at tempus dui.' },
  { title: 'Nulla vitae lorem elit', content: 'Fusce tincidunt imperdiet eros nec fermentum. Aliquam id risus in lacus luctus feugiat nec id magna. Aliquam pretium nibh leo, at tempus felis venenatis non. Etiam hendrerit nec justo eu maximus. Quisque vel lacus sit amet massa fringilla cursus ut id magna. Sed non malesuada sem, id maximus dui.' },
  // Agrega más secciones si es necesario
];

// Función para generar las secciones y actualizar la barra de navegación
function generateSections() {
  const navbar = document.getElementById('navbar');
  const content = document.getElementById('content');

  sections.forEach((sec, index) => {
    const sectionId = `section${index + 1}`;

    // Generar enlace en la barra de navegación
    const link = document.createElement('a');
    link.classList.add('nav');
    link.href = `#${sectionId}`;
    link.onclick = () => scrollToSection(index);
    link.textContent = sec.title;
    const listItem = document.createElement('li');
    listItem.appendChild(link);
    navbar.appendChild(listItem);

    // Crear sección y agregar título y contenido
    const section = document.createElement('section');
    section.id = sectionId;
    const h1 = document.createElement('h1');
    h1.textContent = sec.title;
    section.appendChild(h1);
    const p = document.createElement('p');
    p.innerHTML = sec.content;
    section.appendChild(p);
    content.appendChild(section);
  });
}

// Función para desplazarse a la sección correspondiente
function scrollToSection(index) {
  const sectionId = `section${index + 1}`;
  const section = document.getElementById(sectionId);
  const scrollOptions = {
    behavior: 'smooth',
    block: 'start',
    inline: 'nearest'
  };
  section.scrollIntoView(scrollOptions);
}

// Generar las secciones al cargar la página
window.onload = generateSections;

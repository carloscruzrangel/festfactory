/* AGREGA EVENTOS
 * Este código captura el evento de envío de un formulario de agregar evento,
 * obtiene los valores de los campos del formulario, realiza una solicitud POST
 * a una API utilizando XMLHttpRequest con los datos del evento, y muestra una
 * alerta según el resultado de la solicitud.
 */

const xhr = new XMLHttpRequest();
const sites = document.getElementById('sitio');
const buttonEvent = document.getElementById('accept-event');
const addEventForm = document.getElementById('agregar-evento');
const dueDate = document.getElementById('fecha');
const name = document.getElementById('nombre');
const description = document.getElementById('descripcion');

var today = new Date();
var minDate = today.toISOString().split('T')[0];
dueDate.setAttribute('min', minDate);

//Función para cargar los sitios disponibles para los eventos
function loadSites() {
  xhr.open('GET', url + '/sites', true);
  xhr.setRequestHeader('Authorization', localStorage.getItem('TOKEN'));
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        let siteList = JSON.parse(xhr.responseText);
        if(siteList.length == 0) return;
        sites.innerHTML = '';
        let opt = document.createElement('option');
        opt.value = -1;
        opt.textContent = 'Seleccione un sitio'
        sites.appendChild(opt)
        siteList.forEach(function (site) {
          let option = document.createElement('option');
          option.value = site.id;
          option.textContent = site.name;
          sites.appendChild(option);
        });
      }
    }
  };
  xhr.send();
}

name.addEventListener('input', function() {
  buttonEvent.disabled = !(name.value.trim().length > 0)
});

description.addEventListener('input', function () {
  buttonEvent.disabled = !(description.value.trim().length > 0)
});

// Agregar evento de escucha al formulario con el id 'agregar-evento'
addEventForm.addEventListener('submit', function (event) {
  event.preventDefault(); // Evita el envío del formulario
  buttonEvent.disabled = true;

  // Crear un objeto de datos con los valores obtenidos
  let jsonData = JSON.stringify({
    date: dueDate.value,
    start_time: document.getElementById('hora_inicio').value,
    end_time: document.getElementById('hora_fin').value,
    site: sites.value,
    name: name.value.trim(),
    description: description.value.trim()
  });

  createEvent(jsonData);
});

function createEvent(jsonData) {
  // Configurar la solicitud POST
  xhr.open('POST', url + '/events', true);
  xhr.setRequestHeader('Content-type', 'application/json');
  xhr.setRequestHeader('Authorization', localStorage.getItem('TOKEN'));

  // Manejar el estado de la solicitud
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 201) {
        alert('exito');
        addEventForm.reset();
      } else {
        alert('fail');
        // No login. Hacer algo para que el usuario se dé cuenta
      }
      buttonEvent.disabled = false;
    }
  };

  // Enviar la solicitud POST con los datos en formato JSON
  xhr.send(jsonData);
}

const volverBtn = document.getElementById('volverBtn');
volverBtn.addEventListener('click', function () {
  window.location.href = './listaEventos.html';
});

window.onload = loadSites

const xhr = new XMLHttpRequest();
var key = 'pep3Peca5_PicapaP4s';
let id = decodeURIComponent(new URLSearchParams(window.location.search).get('id'));
let plaintext = CryptoJS.AES.decrypt(id, key).toString(CryptoJS.enc.Utf8);

function getData() {
  xhr.open('GET', url + '/events/' + plaintext, true);
  xhr.setRequestHeader('Authorization', localStorage.getItem('TOKEN'));

  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        loadData(JSON.parse(xhr.responseText))
      } else {
        alert('fail');
      }
    }
  };
  xhr.send();
}

function loadData(json) {
  const placehold = document.getElementById('info-concierto')
  const before = document.getElementById('first-bar')

  var info = document.createElement('p')
  info.innerHTML = json.description
  var name = document.createElement('h2')
  name.textContent = json.name
  var site = document.createElement('p')
  site.innerHTML = json.site.name + '<br>'
  if(json.site.areas.length != 0) {
    json.site.areas.forEach(function (area) {
      site.innerHTML += area.name + '<br>'
    });
  } else {
    site.innerHTML += 'No hay areas asignadas';
  }

  placehold.insertBefore(info, before)
  placehold.insertBefore(name, before)
  placehold.insertBefore(site, before)

  document.getElementById('nplace').textContent = json.site.name;
  document.getElementById('place').textContent = 'LAT: ' + json.site.location.lat + ', LON: ' + json.site.location.lon;
}

document.addEventListener("DOMContentLoaded", function() {
  var openModalBtn = document.getElementById("openModalBtn");
  var closeModalBtn = document.getElementById("closeModalBtn");
  var modal = document.getElementById('modal');
  let modalResponse = document.getElementById('modalResponse');
  let closeModalResponseBtn = document.getElementById('closeModalResponseBtn');
  let response = document.getElementById('response');

  openModalBtn.addEventListener("click", function() {
    modal.style.display = 'block';
  });

  closeModalBtn.addEventListener('click', function() {
    modal.style.display = 'none';
  });

  closeModalResponseBtn.addEventListener("click", function() {
    modalResponse.style.display = 'none';
  });

  window.addEventListener('click', function(event) {
    if (event.target == modal) {
      modal.style.display = 'none';

    }
    if (event.target == modalResponse) {
      modalResponse.style.display = 'none';
    }
  });

  var verificationForm = document.getElementById("verificationForm");
  verificationForm.addEventListener('submit', function(event) {
    event.preventDefault();
    let name = document.getElementById('RegistroField').value;
    let jsonData = JSON.stringify({name: name})
    xhr.open('POST', url + '/events/' + plaintext +'/tickets',true);
    xhr.setRequestHeader('Content-type', 'application/json');
    xhr.setRequestHeader('Authorization', localStorage.getItem('TOKEN'));
    xhr.onreadystatechange = function () {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        modal.style.display = 'none';
        modalResponse.style.display = 'block';
        if (xhr.status === 201) {
          response.innerText = 'Fuiste registrado(a) con éxito, este es el ID de tu boleto: ';
          let p = document.createElement('p');
          let tickets = JSON.parse(xhr.responseText).tickets;
          let ticket = tickets.find((item) => item.name === name);
          p.innerText = ticket.uuid
          document.getElementById('modalResponseContent').appendChild(p);
        } else {
          response.innerText = 'No se pudo obtener el boleto';
        }
      }
    };
    xhr.send(jsonData);
  });
});

window.onload = getData;

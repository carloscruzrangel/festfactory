const xhr = new XMLHttpRequest();

function generateList() {
  var urlParams = new URLSearchParams(window.location.search);
  var id = urlParams.get('id');

  xhr.open('GET', url + '/events/' + id, true);
  xhr.setRequestHeader('Authorization', localStorage.getItem('TOKEN'));
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        var event = JSON.parse(xhr.responseText);
        var table = document.createElement('table');
        var thead = document.createElement('thead');
        var tbody = document.createElement('tbody');

        // Crear encabezado de la tabla
        var headerRow = document.createElement('tr');
        var headers = ['Hora de inicio', 'Hora de fin', 'ID del empleado', 'Nombre del empleado'];
        headers.forEach(function (headerText) {
          var header = document.createElement('th');
          header.textContent = headerText;
          headerRow.appendChild(header);
        });
        thead.appendChild(headerRow);
        table.appendChild(thead);

        // Crear filas de empleados
          var assignments_list = event.assignments;
          //alert(JSON.stringify(assignments_list));
          assignments_list.forEach(function (assignment) {
            var employees = assignment.employees;
            employees.forEach(function (employee) {
              //alert(employee);
              var row = document.createElement('tr');
              var inicioCell = document.createElement('td');
              inicioCell.textContent = assignment.assignment_start;
              var finCell = document.createElement('td');
              finCell.textContent = assignment.assignment_end;
              var idEmpleadoCell = document.createElement('td');
              idEmpleadoCell.textContent = employee.id;
              var nombreEmpleadoCell = document.createElement('td');
              nombreEmpleadoCell.textContent = employee.name + ' ' + employee.first_surname + ' ' + employee.second_surname;

              row.appendChild(inicioCell);
              row.appendChild(finCell);
              row.appendChild(idEmpleadoCell);
              row.appendChild(nombreEmpleadoCell);
              tbody.appendChild(row);
            })
          });

        table.appendChild(tbody);
        var main = document.getElementById('cronograma');
        main.appendChild(table);
      } else {
        //alert(':(');
      }
    }
  };
  xhr.send();
}

// Generate contacts on load
window.onload = generateList;

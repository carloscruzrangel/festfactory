const xhr = new XMLHttpRequest();
var key = 'Un_L4mb0rgh11_vE105';

function generateList() {
  xhr.open('GET', url + '/items', true);
  xhr.setRequestHeader(
    'Authorization',
    localStorage.getItem('TOKEN')
  );
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        generateTable(JSON.parse(xhr.responseText));
      } else {
        //alert(':(');
      }
    }
  };
  xhr.send();
}

function deleteItem(itemId) {
  xhr.open('DELETE', url + '/items/' + itemId, true);
  xhr.setRequestHeader(
    'Authorization',
    localStorage.getItem('TOKEN')
  );
  xhr.onreadystatechange = function () {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        window.location.reload();
      } else {
        // Manejar el caso de error de la solicitud
      }
    }
  };
  xhr.send();
}

function generateTable(json) {
  var table = document.createElement('table');
  var thead = document.createElement('thead');
  var tbody = document.createElement('tbody');

  // Crear encabezado de la tabla
  var headerRow = document.createElement('tr');
  var headers = [/*'ID', */'Nombre', 'Cantidad', '', ''];
  headers.forEach(function (headerText) {
    var header = document.createElement('th');
    header.textContent = headerText;
    headerRow.appendChild(header);
  });
  thead.appendChild(headerRow);
  table.appendChild(thead);

  if(json == null) return;
  // Crear filas de empleados
  json.forEach(function(item){
    var row = document.createElement('tr');
    /*var idCell = document.createElement('td');
    idCell.textContent = item.id;*/
    var nameCell = document.createElement('td');
    nameCell.textContent = item.name;
    var countCell = document.createElement('td');
    countCell.textContent = item.count;
    var updateCell = document.createElement('td');
    var deleteCell = document.createElement('td');

    var updateButton = document.createElement('button');
    updateButton.textContent = 'Editar';
    updateButton.classList.add('botonEditar');

    var deleteButton = document.createElement('button');
    deleteButton.textContent = 'Eliminar';
    deleteButton.classList.add('botonEliminar');

    updateButton.addEventListener('click', function() {
      var encrypted = CryptoJS.AES.encrypt(String(item.id), key);
      window.location.href = './crudArticulo.html?id=' + encodeURIComponent(encrypted).replace(/%20/g, '+');
    });

    deleteButton.addEventListener('click', function () {
      deleteItem(item.id);
    });

    updateCell.appendChild(updateButton);
    deleteCell.appendChild(deleteButton);

    //row.appendChild(idCell);
    row.appendChild(nameCell);
    row.appendChild(countCell);
    row.appendChild(updateCell);
    row.appendChild(deleteCell);
    tbody.appendChild(row);
  });

  table.appendChild(tbody);
  document.getElementById('ListaArticulos').appendChild(table);
}

// Generate contacts on load
window.onload = generateList;


<a name="readme-top"></a>

<br />
<div align="center">
  <br/>
  <a href="">
    <img src="https://cdn.discordapp.com/attachments/1077424670149726388/1111876157344849940/LogoProducto.png" alt="logo" width="110"  height="auto" />
  </a>

  <h3 align="center">PROYECTO:FestFactory</h3>


  </p>
</div>

______

![Status badge](https://img.shields.io/badge/status-TERMINADO%20-green?style=for-the-badge)


<div align="center">
  <img src="https://user-images.githubusercontent.com/79823316/220245622-4cd1cc0b-521c-4252-8e39-1d2b0d9c2389.png" alt="Logo" width="80" height="80">
</div>

____
## Equipo: JAVATAR

<div align="center">
  <img src="https://cdn.discordapp.com/attachments/1009462427257151558/1077273650564177930/Screenshot_20230220-105939-744.png" alt="logo" width="200"  height="auto" />
  <br/>


</div>

### Colaboradores:


* Carlos Cruz Rangel - [Gitlab](https://gitlab.com/carloscruzrangel) - [Github](https://github.com/CarlosCruzRangel) - 312285823 - carloscruzrangel@ciencias.unam.mx

* Juan Manuel Diaz Quiñonez - [Gitlab](https://gitlab.com/Juanma32) - [Github](https://github.com/JuanDiazDev) - 420004365 - juanmanuel32@ciencias.unam.mx

* Jose Demian Jimenez Salgado - [Gitlab](https://gitlab.com/demian035) - [Github](https://github.com/demian35) - 314291707 - josedemian@ciencias.unam.mx

* Toprak Memik Hernandez - [Gitlab](https://gitlab.com/toprakshakur) - [Github](https://github.com/ToprakShakur) - 419002354 - toprakshakur@ciencias.unam.mx

* Raúl Nuño Valdés - [Gitlab](https://gitlab.com/rnunovaldes) - [Github](https://github.com/Rulox710) - 317204580 - rnunovaldes@ciencias.unam.mx


<p align="right">(<a href="#readme-top">back to top</a>)</p>

____

# Descripción del proyecto
## Introducción
Fest Factory ayuda a las empresas planificar, administrar y ejecutar eventos exitosos de manera eficiente y rentable. Ofrecemos una solución completa para la gestión de inventario, la planificación de la disposición física del equipo en el lugar del evento y la asignación de empleados. Además, nuestros clientes pueden entrar al portal público para que los participantes se registren en el evento y lo empleados puedan hacer al invitado check-in una vez lleguen al lugar del evento. Con nuestra plataforma, las empresas pueden ahorrar tiempo y recursos valiosos mientras entregan eventos de alta calidad y atractivos para los asistentes. Déjalo a nosotros y ¡disfruta del show!

## Objetivo del proyecto
* Poder organizar y planear perfectamente tu evento desde cualquier lugar que tenga conexiona a internet

* Plataforma muy accesible y fácil de usar para cualquier persona.

* Puedes planificar cualquier tipo de evento
## Requerimientos del proyecto
* Gestión de empleados de la empresa.

* Gestión de inventario.

* Asignación de empleados a sitios.

* Cronograma de asignación para los empleados.

* Cronogramas para la renta de equipos o insumos para los eventos.

* Mapas de calor de eventos a nivel geográfico.

* Un portal público para que los participantes se puedan registrar en los eventos

* Un método para verificar el “boleto” asignado al usuario una vez que se está en el evento.

____
# Sobre la implementación

## Árbol de Carpetas

## Instancias usadas en el proyecto
### Lenguajes de Programación usado:
* ![JavaScript](https://img.shields.io/badge/JavaScript-323330?style=for-the-badge&logo=javascript&logoColor=F7DF1E)

* ![CSS](https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white)

* ![HTML5](https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white)

### Editor de código usado:

* ![VsCode](https://img.shields.io/badge/VSCode-0078D4?style=for-the-badge&logo=visual%20studio%20code&logoColor=white)

### Web Browser usados:

* ![Chrome](https://img.shields.io/badge/Google_chrome-4285F4?style=for-the-badge&logo=Google-chrome&logoColor=white)

* ![Edge](https://img.shields.io/badge/Microsoft_Edge-0078D7?style=for-the-badge&logo=Microsoft-edge&logoColor=white)

* ![Safari](https://img.shields.io/badge/Safari-FF1B2D?style=for-the-badge&logo=Safari&logoColor=white)

### Control de versiones usado:

* ![Gitlab](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white)

* ![Gitlab](https://img.shields.io/badge/GIT-E44C30?style=for-the-badge&logo=git&logoColor=white)

### Sistema Operativos usados:

* ![win11](https://img.shields.io/badge/Windows_11-0078d4?style=for-the-badge&logo=windows-11&logoColor=white)

* ![win10](https://img.shields.io/badge/Windows-0078D6?style=for-the-badge&logo=windows&logoColor=white)

* ![ubuntu](https://img.shields.io/badge/Ubuntu-E95420?style=for-the-badge&logo=ubuntu&logoColor=white)

* ![Mac](https://img.shields.io/badge/mac%20os-000000?style=for-the-badge&logo=apple&logoColor=whit)


____

# Como Comenzar

## Descarga del proyecto

### Clonando el repositorio
1. Has click [aqui](https://gitlab.com/carloscruzrangel/festfactory) para poder aceder al repositorio, o copia el sigueinte url y pegarlo en tu navegador: https://gitlab.com/carloscruzrangel/festfactory

![CapturaDeUsoDescragaZip](https://cdn.discordapp.com/attachments/1111905104346501150/1111905609214857286/CapturaDeUsoCLone.png)

![CapturaDeUsoDescragaZip](https://cdn.discordapp.com/attachments/1111905104346501150/1111908334967205919/CapturaDeUsoCLone2.png)

2. Elige el método de clonación de tu preferencia.

3. Termiando de clonar ve a la carpeta carpeta destino de clonacion, ahí encontraras el archivo

Nota: La rama del proyecto es la rama `main`.


### Descagando como Archivo Comprimido
1. Has click [aqui](https://gitlab.com/carloscruzrangel/festfactory) para poder aceder al repositorio, o copia el sigueinte url y pegarlo en tu navegador: https://gitlab.com/carloscruzrangel/festfactory

2. Has click en el botón de descarga, como está en la imagen:

![CapturaDeUsoDescragaZip](https://cdn.discordapp.com/attachments/1111905104346501150/1111905144511139900/CapturaDeUsoDescragaZip.png)

![CapturaDeUsoDescragaZip](https://cdn.discordapp.com/attachments/1111905104346501150/1111906406807916634/CapturaDeUsoDescragaZip2.png)

3. Elije el formato de tu preferencia y pulsa, la descarga comenzara de inmediato

4. Ve a la carpeta destino de tus descargas ahí encontraras el archivo.

Nota la rama que tendras que descargar será la rama `main`
## Como lanzar la pagina
1. Ve a la carpeta del archivo que descargaste o clonaste, si lo descargaste en un archivo comprimido entonces tendras que descomprimirlo, pero si lo clonaste no es necesaria la descompresión debido a que se descarga como una carpeta en tu memoria local.

2. Después abriras la carpeta, si fue clonado la carpeta se llamará "festfactory", pero si fue descargado en archivo comprimido la carpeta que abriras se llamará "festfactory-main".

3. Ahora buscaras la carpeta "templates", después la abriras.

4. Ahora buscaras el archivo `llamado login.html` (En algunos sistemas operativos no te muesta la extención del archivo y puede que simplemente te aparezca "login", asi que fijate que sea un archivo que pueda abrir tu explorador de internet directamente ya que en esta carpeta hay una imagen llamada login la cual es un archivo `.png` el cual no es el archivo objetivo de este paso). Darás doble clic en el archivo para abrirlo en tu explorador de internet. Tu navegador lanzara la siguiente página:

![CapturaDeUso2](https://cdn.discordapp.com/attachments/1111905104346501150/1112143972584931408/mockcompu2.png)

## Cómo iniciar sesión
En el `login` tendras que poner lo siguiente:

```
User:admin@diu-server.net
Password:m{|3)Al#cRxcf,4!ePK~~KvV:InEmAy!g
```

Despues debes oprimir "Sign in"
![CapturaDeUso2](https://cdn.discordapp.com/attachments/1111905104346501150/1111945364321357894/image.png)

Después debes oprimir "Sign in", al hacerlo y si los datos son correctos te llevará a la siguiente página:

![CapturaDeUso2](https://cdn.discordapp.com/attachments/1111905104346501150/1112143781777649694/mocknave1.png)

# NOTAS DEL PROYECTO
1. La verifición del checkin no se puede mantener debido a la api, tampoco se puede eliminar por que no puedes eliminar un boleto.

2. El portal en realidad no puede ser publico por que para visualizar eventos pide un token válido y este es privado.

3. Al hacer una petición del asigment no podia acceder al nombre de los empleados solo regresa un id raro.
____
## Comentarios y retos sobre el Proyecto

### Comentario de: Carlos Cruz Rangel
> Programar una página web con HTML, CSS y Javascript ha sido todo un desafío para mí. Desde el principio, me encontré con dificultades para alinear los elementos correctamente y lograr el diseño que tenía en mente. Pero eso no me desanimó, me impulsó a aprender más y a buscar soluciones creativas. Una de las cosas más gratificantes de programar mi página web ha sido ver cómo evolucionaba con el tiempo. Empecé con una simple estructura HTML y algunos estilos básicos, pero poco a poco fui agregando interacciones con Javascript y refinando el diseño con CSS. Cada pequeña mejora nos ha permitido dar un paso más hacia la página web que imaginamos desde el principio.

### Comentario de: Juan Manuel Diaz Quiñonez
> Durante este proceso, he aprendido que la paciencia es clave. Hubo momentos en los que me encontré atrapado en un bucle infinito de errores y bugs que parecían no tener fin. Pero cada vez que superaba uno de esos obstáculos, sentía satisfacción y confianza en mis habilidades como programador, a veces solo debes creer que puedes hacerlo para hacerlo.

### Comentario de: Jose Demian Jimenez Salgado
> Uno de los mayores desafíos que enfrenté fue lograr que mi página web fuera compatible con diferentes navegadores y dispositivos. Me encontré con problemas de compatibilidad y tuve que hacer ajustes específicos para garantizar una experiencia consistente para todos los usuarios. Esto me hizo darme cuenta de la importancia de la accesibilidad y de asegurarme de que mi sitio web fuera accesible para todos.

### Comentario de: Toprak Memik Hernandez
> Una de las enseñanzas más valiosas que he obtenido al programar una página web es la importancia de la organización y la estructura del código. Al principio, mi código era un completo desorden y me costaba entenderlo incluso yo mismo. Pero con el tiempo, aprendí a dividirlo en bloques más pequeños, a comentar y a nombrar correctamente las clases y los IDs, lo que facilitó enormemente el mantenimiento y la escalabilidad del proyecto.

### Comentario de: Raúl Nuño Valdés
> Durante el proceso de programación, he descubierto la belleza de la creatividad combinada con la lógica. Es como ser un artista y un científico al mismo tiempo. Por un lado, tienes que pensar en cómo hacer que tu página se vea atractiva y funcional, y por otro lado, debes asegurarte de que el código sea limpio y eficiente. Es un equilibrio delicado, pero muy satisfactorio cuando lo logras.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

____



____
